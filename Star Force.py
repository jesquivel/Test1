import os
import random
import time
import winsound
from threading import Thread
from tkinter import *

import pygame as pg

global name_char, bullet_ex
name_char = []
bullet_ex = False

Instrucciones = '''Instrucciones'''
About = '''Instituto Tecnologico de Costa Rica
Ingenieria en Computadores

Lenguaje: Python 3.6.5

Nombre:Jonathan Esquivel Sanchez, Iván Solís Ávila
Carnet:2018167983, 2018209698
                                            
Profesor:Milton Villegas Lemus

Star Force

Version: 1.0.0

Hay dos modalidades de juego:
1. Destrucción de asteroides: se deben destruir los asteroides para acumular puntos
2. Prueba de Maniobras: se debe pasar por el centro de los aros para acumular puntos'''


# ---------------------------------------------Función de imágenes---------------------------------------------
def load_img(name):
    ruta = os.path.join('Img', name)
    img = PhotoImage(file=ruta)
    return img


# --------------------------------------------Función de canciones--------------------------------------------
def load_song(name):
    ruta = os.path.join('Canciones', name)
    return winsound.PlaySound(ruta, winsound.SND_ASYNC)


def off():
    winsound.PlaySound(None, winsound.SND_ASYNC)


# --------------------------------------------Función de canciones--------------------------------------------

def high_scores():
    """
        Instituto Tecnológico de Costa Rica
        Ingeniería en Computadores
        Lenguaje: Python 3.6.5
        Autor: Iván Solís Ávila 2018209698, Jonathan Esquivel Sánchez 2018167983
        Versión: 1.0.0
        Fecha de última modificación: 04/06/2018
        Entradas: no presenta
        Restricciones: no presenta
        Salidas: archivo de texto con las mejores puntuaciones y ventana que las presenta"""
    global score_player, name_char, Dictionary_Characters
    hs = open('high_scores.txt', 'r+')
    HS = eval(hs.readline())
    if not HS[0]:
        print(score_player, name_char)
        HS[0] += [Dictionary_Characters[name_char[0]]]
        HS[1] += [score_player]
        hs.seek(0)
        hs.write(str(HS))
        hs.close
    else:
        print(score_player)
        HS[0] += [Dictionary_Characters[name_char[0]]]
        HS[1] += [score_player]
        DHS = create_dict(HS, 0, {})
        HS[1] = quick_sort(HS[1])
        HS[0] = HS[1]
        HS = high_score_aux(0, DHS, HS, len(HS[0]), HS[1], [])
        hs.seek(0)
        print(HS)
        hs.write(str(HS)+(" "*2000))
        hs.close


def high_score_aux(c, DHS, HS, L, SubS, SubN):
    if c == L:
        if L == 5:
            print(SubS,SubN)
            SubS = SubS[1:]
            SubN = SubN[1:]
            print(SubS, SubN)
            return [SubN, SubS]
        else:
            return [SubN, SubS]
    else:
        SubN += [DHS[HS[0][c]][0]]
        DHS[HS[1][c]] = DHS[HS[1][c]][1:]
        return high_score_aux(c + 1, DHS, HS, L, SubS, SubN)

def create_dict(HS, N, Res): #Recursiva
    if N == len(HS[0]):
        return Res
    else:
        try:
            Res[HS[1][N]] += [HS[0][N]]
            return create_dict(HS, N+1, Res)
        except:
            Res[HS[1][N]] = [HS[0][N]]
            return create_dict(HS, N+1, Res)

def quick_sort(Lista):
    Menores = []
    Iguales = []
    Mayores = []
    if len(Lista) <= 1:
        return Lista
    else:
        Pivote = Lista[-1]
        partir(Lista, 0, len(Lista), Pivote, Menores, Iguales, Mayores)
        NL = quick_sort(Menores)
        NL.extend(Iguales)
        NL.extend(quick_sort(Mayores))
        return NL

def partir(Lista, i, n, Pivote, Menores, Iguales, Mayores):
    if i == n:
        return Menores, Iguales, Mayores
    if Lista[i] < Pivote:
        Menores.append(Lista[i])
        return partir(Lista, i + 1, n, Pivote, Menores, Iguales, Mayores)
    elif Lista[i] > Pivote:
        Mayores.append(Lista[i])
        return partir(Lista, i + 1, n, Pivote, Menores, Iguales, Mayores)
    else:
        Iguales.append(Lista[i])
        return partir(Lista, i + 1, n, Pivote, Menores, Iguales, Mayores)


# --------------------------------------Pantalla de presentación (root)---------------------------------------
root = Tk()
root.title("F-Zero")
root.minsize(900, 506)
root.resizable(width=NO, height=NO)

c_root = Canvas(root, width=900, height=506, bg='black')
c_root.place(x=0, y=0)

background_root = load_img('Fondo.gif')
background = Label(c_root, image=background_root)
background.place(x=0, y=0, relwidth=1, relheight=1)
load_song("Menu.wav.wav")


# ------------------------------------------Pantalla config y juegos------------------------------------------
def config():
    """
        Instituto Tecnológico de Costa Rica
        Ingeniería en Computadores
        Lenguaje: Python 3.6.5
        Autor: Iván Solís Ávila 2018209698, Jonathan Esquivel Sánchez 2018167983
        Versión: 1.0.0
        Fecha de última modificación: 04/06/2018
        Entradas: no presenta
        Restricciones: no presenta
        Salidas: ventana de confiuración antes de iniciar el juego"""
    off()
    load_song("Config.wav.wav")
    global Dictionary_Characters, name_char, List_Characters
    root.withdraw()
    config = Toplevel()
    config.title('Configuración')
    config.minsize(800, 491)
    config.resizable(width=NO, height=NO)

    c_config = Canvas(config, width=800, height=491, bg='black')
    c_config.place(x=0, y=0)

    c_characters = Canvas(config, width=400, height=200, bg='black')
    c_characters.place(x=200, y=25)

    E_name = Entry(config, width=40)
    E_name.place(x=300, y=435)

    background_config = load_img('Fondo_config.png')
    Game_Over = load_img('Game_Over.png')
    Amy = load_img('Amy.gif')
    Bayonetta = load_img('Bayonetta.gif')
    Blood_Falcon = load_img('Blood_Falcon.gif')
    Captain_Falcon = load_img('Captain_Falcon.gif')
    DK = load_img('DK.gif')
    Falcon = load_img('Falcon.gif')
    Fox = load_img('Fox.gif')
    Krystal = load_img('Krystal.gif')
    Marth = load_img('Marth.gif')
    Nights = load_img('Nights.gif')
    Peppy = load_img('Peppy.gif')
    Pico = load_img('Pico.gif')
    ROB = load_img('ROB.gif')
    Samus = load_img('Samus.gif')
    Slippy = load_img('Slippy.gif')
    Snake = load_img('Snake.gif')
    Sonic = load_img('Sonic.gif')
    Strife = load_img('Strife.gif')
    Wolf = load_img('Wolf.gif')
    ZS_Samus = load_img('ZS_Samus.gif')

    List_Characters = [Amy, Bayonetta, Blood_Falcon, Captain_Falcon, DK, Falcon, Fox, Krystal, Marth, Nights,
                       Peppy, Pico, ROB, Samus, Slippy, Snake, Sonic, Strife, Wolf, ZS_Samus]

    Dictionary_Characters = {Amy: 'Amy', Bayonetta: 'Bayonetta', Blood_Falcon: 'Blood Falcon',
                             Captain_Falcon: 'Captain Falcon', DK: 'Donkey Kong', Falcon: 'Falcon', Fox: 'Fox',
                             Krystal: 'Krystal', Marth: 'Marth', Nights: 'Nights', Peppy: 'Peppy', Pico: 'Pico',
                             ROB: 'ROB',
                             Samus: 'Samus', Slippy: 'Slippy', Snake: 'Snake', Sonic: 'Sonic', Strife: 'Strife',
                             Wolf: 'Wolf',
                             ZS_Samus: 'Zero Suit Samus'}

    c_config.create_image(0, 0, image=background_config, anchor=NW)

    def back():
        config.destroy()
        root.deiconify()
        off()
        load_song("Menu.wav.wav")

    def select_chr(name_chr, c):
        global character, name_char
        character = name_chr
        c_config.create_image(202, 375, image=name_chr, anchor=NW)
        E_name.delete(0, END)
        E_name.insert(0, Dictionary_Characters[name_chr])
        name_char = [name_chr, c]

    def commit_changes():
        global name_char
        Dictionary_Characters[name_char[0]] = E_name.get()

    def place_btn(c, x, y):  # Recursiva
        """
            Instituto Tecnológico de Costa Rica
            Ingeniería en Computadores
            Lenguaje: Python 3.6.5
            Autor: Iván Solís Ávila 2018209698, Jonathan Esquivel Sánchez 2018167983
            Versión: 1.0.0
            Fecha de última modificación: 04/06/2018
            Entradas: posición (c) del personaje deseado en la lista, posición en el grid de la ventana
            Restricciones: no presenta
            Salidas: ventana de información"""
        if x == 5 and y == 3:
            pass
        elif x == 5:
            place_btn(c, 0, y + 1)
        else:
            btn_characters = Button(c_characters, image=List_Characters[c],
                                    command=lambda: select_chr(List_Characters[c], c), bg='darkblue')
            btn_characters.grid(row=y, column=x)
            place_btn(c + 1, x + 1, y)

    def score():
        off()
        load_song("Score.wav.wav")
        score = Toplevel()
        score.title('Score')
        score.minsize(800, 600)
        score.resizable(width=NO, height=NO)

        c_score = Canvas(score, width=800, height=600, bg='black')
        c_score.place(x=0, y=0)

        def random_score(N, Res):  # Recursiva
            global random_score_char, score_player
            if N == 19:
                Res += [score_player]
                Res = quick_sort(Res)
                random_score_char = Res
            else:
                Res += [random.randint(0, 9)]
                random_score(N + 1, Res)

        def quick_sort(Lista):  # Recursiva
            Menores = []
            Iguales = []
            Mayores = []
            if len(Lista) <= 1:
                return Lista
            else:
                Pivote = Lista[-1]
                partir(Lista, 0, len(Lista), Pivote, Menores, Iguales, Mayores)
                NL = quick_sort(Menores)
                NL.extend(Iguales)
                NL.extend(quick_sort(Mayores))
                return NL

        def partir(Lista, i, n, Pivote, Menores, Iguales, Mayores):
            if i == n:
                return Menores, Iguales, Mayores
            if Lista[i] < Pivote:
                Menores.append(Lista[i])
                return partir(Lista, i + 1, n, Pivote, Menores, Iguales, Mayores)
            elif Lista[i] > Pivote:
                Mayores.append(Lista[i])
                return partir(Lista, i + 1, n, Pivote, Menores, Iguales, Mayores)
            else:
                Iguales.append(Lista[i])
                return partir(Lista, i + 1, n, Pivote, Menores, Iguales, Mayores)

        random_score(0, [])

        def place_lbl(c, n, x, y, List_Characters):  # Recursiva
            """
               Instituto Tecnológico de Costa Rica
               Ingeniería en Computadores
               Lenguaje: Python 3.6.5
               Autor: Iván Solís Ávila 2018209698, Jonathan Esquivel Sánchez 2018167983
               Versión: 1.0.0
               Fecha de última modificación: 04/06/2018
               Entradas: cantidad de personajes, posición de las puntuaciones, coordenadas, lista de personajes
               Restricciones: no presenta
               Salidas: creación de las labels en la pantalla de scores"""
            global score_player, name_char
            if x == 590 and y == 600:
                pass
            elif y == 600:
                place_lbl(c, n, x + 180, 90, List_Characters)
            elif random_score_char[n] == score_player:
                lbl_player_name = Label(c_score, text=Dictionary_Characters[name_char[0]], bg='black', fg='white')
                lbl_player_score = Label(c_score, text='Score: ' + str(score_player), bg='black', fg='white')
                lbl_player = Label(c_score, image=name_char[0], bg='darkblue')
                name_char = []
                lbl_player.place(x=x, y=y)
                lbl_player_name.place(x=x + 90, y=y)
                lbl_player_score.place(x=x + 90, y=y + 50)
                score_player = -1
                place_lbl(c, n - 1, x, y + 102, List_Characters)
            elif c == 0:
                lbl_name = Label(c_score, text=Dictionary_Characters[List_Characters[0]], bg='black', fg='white')
                lbl_score = Label(c_score, text="Score: " + str(random_score_char[n]), bg='black', fg='white')
                lbl_characters = Label(c_score, image=List_Characters[0], bg='darkblue')

                lbl_name.place(x=x + 90, y=y)
                lbl_score.place(x=x + 90, y=y + 50)
                lbl_characters.place(x=x, y=y)
                place_lbl(c - 1, n - 1, x, y + 102, List_Characters)
            else:
                h = random.randint(0, c)
                lbl_name = Label(c_score, text=Dictionary_Characters[List_Characters[h]], bg='black', fg='white')
                lbl_score = Label(c_score, text="Score: " + str(random_score_char[n]), bg='black', fg='white')
                lbl_characters = Label(c_score, image=List_Characters[h], bg='darkblue')
                CP = List_Characters[0]
                List_Characters[0] = List_Characters[h]
                List_Characters[h] = CP
                lbl_characters.place(x=x, y=y)
                lbl_name.place(x=x + 90, y=y)
                lbl_score.place(x=x + 90, y=y + 50)
                place_lbl(c - 1, n - 1, x, y + 102, List_Characters[1:])

        lbl_Game_Over = Label(c_score, image=Game_Over, bg='black')
        lbl_Game_Over.place(x=167, y=8)

        place_lbl(18, 19, 50, 90, List_Characters)

        def back_score():
            score.destroy()
            root.deiconify()
            off()
            load_song("Menu.wav.wav")

        btn_back_score = Button(c_score, text='Atrás', command=back_score, bg='black', fg='white')
        btn_back_score.place(x=750, y=550)

    # --------------------------------------------------Ring Game-------------------------------------------------
    def Game_ring():
        """
            Instituto Tecnológico de Costa Rica
            Ingeniería en Computadores
            Lenguaje: Python 3.6.5
            Autor: Iván Solís Ávila 2018209698, Jonathan Esquivel Sánchez 2018167983
            Versión: 1.0.0
            Fecha de última modificación: 04/06/2018
            Entradas: no presenta
            Restricciones: no presenta
            Salidas: Juego de Aros"""
        config.withdraw()
        pg.init()
        global pos_ring_rt, ce2, HBar, score_player, ON, hit, hit_bat

        gameDisplay = pg.display.set_mode((800, 600))
        pg.display.set_caption('F-Zero')
        clock = pg.time.Clock()

        game_bg = pg.image.load(os.path.join('Img', 'Fondo_juego.jpg'))
        cntr = pg.image.load(os.path.join('Img', 'C.gif'))
        left1 = pg.image.load(os.path.join('Img', 'L1.gif'))
        left2 = pg.image.load(os.path.join('Img', 'L2.gif'))
        right1 = pg.image.load(os.path.join('Img', 'R1.gif'))
        right2 = pg.image.load(os.path.join('Img', 'R2.gif'))

        img_ring = pg.image.load(os.path.join('Img', 'Ring.gif'))

        HB0 = pg.image.load(os.path.join('Img', 'HB0.gif'))
        HB1 = pg.image.load(os.path.join('Img', 'HB1.gif'))
        HB2 = pg.image.load(os.path.join('Img', 'HB2.gif'))
        HB3 = pg.image.load(os.path.join('Img', 'HB3.gif'))
        HB4 = pg.image.load(os.path.join('Img', 'HB4.gif'))
        HB5 = pg.image.load(os.path.join('Img', 'HB5.gif'))
        HB6 = pg.image.load(os.path.join('Img', 'HB6.gif'))

        battery = pg.image.load(os.path.join('Img', 'Battery.gif'))

        Healthbar = {0: HB0, 1: HB1, 2: HB2, 3: HB3, 4: HB4, 5: HB5, 6: HB6}
        HBar = 6
        RTHB = Healthbar[HBar]

        Hbar_aux = 0
        score_player = 0

        def create_ring(ce):
            global pos_ring, pos_ring_rt, ce1, ring, posx_ring, posy_ring, ring_ex, ring_des, ON, hit, hit_bat
            ce = int(ce)
            pos_ring = 0
            x = random.randint(0, 600)
            posx_ring = x
            y = random.randint(0, 400)
            posy_ring = y
            ring = img_ring
            pos_ring_rt = [x, y, ring, 0, 0]
            ce1 = ce
            ring_size = 300
            ring_ex = True
            ring_des = False
            mult = 0.3
            if ce == 0:
                ON = False
            else:
                while pos_ring < 5 and ON:
                    if pos_ring + 1 == 5:
                        hit = True
                    time.sleep(0.7)
                    pos_ring_rt = [x, y, ring, int(ring_size * mult), int(ring_size * mult)]
                    pos_ring += 1
                    pg.display.flip()
                    mult += 0.2
                    if not ON:
                        sys.exit()
                pos_ring_rt = [x, y, ring, 0, 0]
                create_ring(ce - 1)

        def create_bat():
            global pos_bat, pos_bat_rt, posx_bat, posy_bat, hit_bat
            T = random.randint(0, 4)
            pos_bat = 0
            x = random.randint(0, 600)
            posx_bat = x
            y = random.randint(0, 400)
            posy_bat = y
            pos_bat_rt = [x, y, battery, 0, 0]
            mult = 0.3
            time.sleep(T)
            while pos_bat < 5 and ON:
                if pos_bat + 1 == 5:
                    hit_bat = True
                pos_bat_rt = [x, y, battery, int(40 * mult), int(21 * mult)]
                pos_bat += 1
                pg.display.flip()
                time.sleep(0.5)
                mult += 0.2
            if not ON:
                sys.exit()
            pos_bat_rt = [x, y, battery, 0, 0]
            create_bat()

        Thread(name='Create rings', target=create_ring, args='9').start()
        Thread(name='Create batteries', target=create_bat).start()

        ON = True
        hit_bat = False
        myfont = pg.font.SysFont('Times New Roman', 20)
        while ON:
            global ring, pos_ring, ring_ex, pos_bat, pos_bat_rt, posx_bat, posy_bat, pos_ring_destroy, \
                pos_bullet_rt, hit
            mouse = pg.mouse.get_pos()
            gameDisplay.blit(game_bg, (0, 0))
            gameDisplay.blit(RTHB, (0, 0))
            Hbar_aux += 1
            text_sc = myfont.render('Puntaje: ' + str(score_player), False, (255, 255, 255))
            gameDisplay.blit(text_sc, (700,500))
            text_ce = myfont.render('Objetivos restantes: ' + str(ce1), False, (255, 255, 255))
            gameDisplay.blit(text_ce, (605,450))
            if ce1 != 0 and ring_ex:
                scale = pg.transform.scale(pos_ring_rt[2], (pos_ring_rt[3], pos_ring_rt[4]))
                gameDisplay.blit(scale, (pos_ring_rt[0], pos_ring_rt[1]))
            if not ring_ex:
                if pos_bullet_rt[3] != 0:
                    scale = pg.transform.scale(pos_ring_destroy[2], (pos_ring_destroy[3], pos_ring_destroy[4]))
                    gameDisplay.blit(scale, (pos_ring_destroy[0], pos_ring_destroy[1]))
                    bullet_scale = pg.transform.scale(pos_bullet_rt[2], (pos_bullet_rt[3], pos_bullet_rt[4]))
                    gameDisplay.blit(bullet_scale, (pos_bullet_rt[0], pos_bullet_rt[1]))
                else:
                    pass
            if ON:
                scale_bat = pg.transform.scale(pos_bat_rt[2], (pos_bat_rt[3], pos_bat_rt[4]))
                gameDisplay.blit(scale_bat, (pos_bat_rt[0], pos_bat_rt[1]))
            if HBar == 0:
                ON = False
                time.sleep(1)
            if Hbar_aux == 210:
                HBar -= 1
                RTHB = Healthbar[HBar]
                Hbar_aux = 0
            if pos_bat == 5 and hit_bat:
                hit_bat = False
                if mouse[0] in range(posx_bat - 73, posx_bat + 73) and mouse[1] in range(posy_bat - 70, posy_bat + 70):
                    HBar += 1
                    if HBar >= 6:
                        HBar = 6
                        RTHB = Healthbar[HBar]
                    else:
                        RTHB = Healthbar[HBar]
            if pos_ring == 5 and hit:
                posx_sqr_left = posx_ring + 70
                posx_sqr_right = posx_ring + 224
                posy_sqr_up = posy_ring + 70
                posy_sqr_down = posy_ring + 224
                hit = False
                if mouse[0] in range(posx_sqr_left, posx_sqr_right) and mouse[1] in range(posy_sqr_up, posy_sqr_down):
                    HBar += 1
                    score_player += 1
                    if HBar >= 6:
                        HBar = 6
                        RTHB = Healthbar[HBar]
                    else:
                        RTHB = Healthbar[HBar]
                else:
                    if mouse[0] in range(posx_ring, posx_sqr_left) and mouse[1] in range(posy_sqr_down,
                                                                                         posy_ring + 294) and hit:
                        HBar -= 1
                        RTHB = Healthbar[HBar]
                    elif mouse[0] in range(posx_sqr_right, posx_ring + 294) and mouse[1] in range(posy_sqr_down,
                                                                                                  posy_ring + 294):
                        HBar -= 1
                        RTHB = Healthbar[HBar]
                    elif mouse[0] in range(posx_ring, posx_sqr_left) and mouse[1] in range(posy_ring, posy_sqr_up):
                        HBar -= 1
                        RTHB = Healthbar[HBar]
                    elif mouse[0] in range(posx_sqr_right, posx_ring + 294) and mouse[1] in range(posy_ring,
                                                                                                  posy_sqr_up):
                        HBar -= 1
                        RTHB = Healthbar[HBar]
                    else:
                        pass
            if 0 <= pg.mouse.get_pos()[0] < 160:
                gameDisplay.blit(left2, mouse)
            if 160 <= pg.mouse.get_pos()[0] < 320:
                gameDisplay.blit(left1, mouse)
            if 320 <= pg.mouse.get_pos()[0] < 480:
                gameDisplay.blit(cntr, mouse)
            if 480 <= pg.mouse.get_pos()[0] < 640:
                gameDisplay.blit(right1, mouse)
            if 640 <= pg.mouse.get_pos()[0] <= 800:
                gameDisplay.blit(right2, mouse)
            for event in pg.event.get():
                if event.type == pg.QUIT:
                    ON = False
            pg.display.update()
            clock.tick(60)

        time.sleep(1.2)
        pg.quit()
        config.destroy()
        high_scores()
        score()

    def Game_ring_v():
        global List_Characters
        if not name_char:
            pass
        else:
            CP = List_Characters[0]
            List_Characters[0] = List_Characters[name_char[1]]
            List_Characters[name_char[1]] = CP
            List_Characters = List_Characters[1:]
            Game_ring()

    # ------------------------------------------------Asteroid Game-----------------------------------------------

    def Game_ast():
        """
            Instituto Tecnológico de Costa Rica
            Ingeniería en Computadores
            Lenguaje: Python 3.6.5
            Autor: Iván Solís Ávila 2018209698, Jonathan Esquivel Sánchez 2018167983
            Versión: 1.0.0
            Fecha de última modificación: 04/06/2018
            Entradas: no presenta
            Restricciones: no presenta
            Salidas: juego de asteroides"""
        config.withdraw()
        pg.init()
        global pos_ast_rt, ce1, HBar, RTHB, bullet_ex, ast_ex, hit, random_score_char, score_player, List_Characters, \
            ON, hit_bat

        gameDisplay = pg.display.set_mode((800, 600))
        pg.display.set_caption('F-Zero')
        clock = pg.time.Clock()

        game_bg = pg.image.load(os.path.join('Img', 'Fondo_juego.jpg'))
        cntr = pg.image.load(os.path.join('Img', 'C.gif'))
        left1 = pg.image.load(os.path.join('Img', 'L1.gif'))
        left2 = pg.image.load(os.path.join('Img', 'L2.gif'))
        right1 = pg.image.load(os.path.join('Img', 'R1.gif'))
        right2 = pg.image.load(os.path.join('Img', 'R2.gif'))

        ast1 = pg.image.load(os.path.join('Img', 'Asteroid1.png'))
        ast2 = pg.image.load(os.path.join('Img', 'Asteroid2.png'))
        ast3 = pg.image.load(os.path.join('Img', 'Asteroid3.png'))
        ast4 = pg.image.load(os.path.join('Img', 'Asteroid4.png'))
        ast_dest = pg.image.load(os.path.join('Img', 'Asteroid_pieces.png'))

        HB0 = pg.image.load(os.path.join('Img', 'HB0.gif'))
        HB1 = pg.image.load(os.path.join('Img', 'HB1.gif'))
        HB2 = pg.image.load(os.path.join('Img', 'HB2.gif'))
        HB3 = pg.image.load(os.path.join('Img', 'HB3.gif'))
        HB4 = pg.image.load(os.path.join('Img', 'HB4.gif'))
        HB5 = pg.image.load(os.path.join('Img', 'HB5.gif'))
        HB6 = pg.image.load(os.path.join('Img', 'HB6.gif'))

        battery = pg.image.load(os.path.join('Img', 'Battery.gif'))
        bullet = pg.image.load(os.path.join('Img', 'Bala.png'))

        Healthbar = {0: HB0, 1: HB1, 2: HB2, 3: HB3, 4: HB4, 5: HB5, 6: HB6}
        HBar = 6
        RTHB = Healthbar[HBar]
        Hbar_aux = 0

        bullet_ex = False
        ast_ex = True
        hit_bat = False

        score_player = 0

        def create_ast(ce):
            global pos_ast, pos_ast_rt, ce1, ast, posx_ast, posy_ast, ast_ex, hit, ON
            print(ce)
            ce = int(ce)
            pos_ast = 0
            mult_ast = 0.3
            x = random.randint(0, 600)
            posx_ast = x
            y = random.randint(0, 400)
            posy_ast = y
            ast = random.choice([ast1, ast2, ast3, ast4])
            pos_ast_rt = [x, y, ast, 0, 0]
            ce1 = ce
            time.sleep(2)
            ast_ex = True
            if ce == 0:
                ON = False
            else:
                while pos_ast < 10 and ast_ex and ON:
                    if pos_ast+1 == 10:
                        pos_ast_rt = [x, y, ast, int(132 * mult_ast), int(127 * mult_ast)]
                        pg.display.flip()
                        mult_ast += 0.08
                        pos_ast += 1
                        hit = True
                        time.sleep(0.3)
                    else:
                        pos_ast_rt = [x, y, ast, int(132 * mult_ast), int(127 * mult_ast)]
                        pos_ast += 1
                        pg.display.flip()
                        mult_ast += 0.08
                        time.sleep(0.3)
                while mult_ast < 1.1 and ON:
                    time.sleep(0.5)
                    mult_ast += 0.08
                pos_ast_rt = [x, y, ast, 0, 0]
                if ON:
                    create_ast(ce - 1)
                else:
                    pass

        def create_bat():
            global pos_bat, pos_bat_rt, posx_bat, posy_bat, ON, hit_bat
            T = random.randint(0, 4)
            pos_bat = 0
            x = random.randint(0, 600)
            posx_bat = x
            y = random.randint(0, 400)
            posy_bat = y
            pos_bat_rt = [x, y, battery, 0, 0]
            time.sleep(T)
            mult=0.3
            while pos_bat < 5 and ON:
                if pos_bat + 1 == 5:
                    hit_bat = True
                pos_bat_rt = [x, y, battery, int(40 * mult), int(21 * mult)]
                pos_bat += 1
                pg.display.flip()
                time.sleep(1)
                mult += 0.2
            pos_bat_rt = [x, y, battery, 0, 0]
            if ON:
                create_bat()
            else:
                pass

        def bullet_move(pos_bullet, mult, x, y):
            global pos_bullet_rt, pos_ast_destroy, bullet_ex, ON
            pos_bullet = int(pos_bullet)
            mult = float(mult)
            x = int(x)
            y = int(y)
            if pos_bullet == pos_ast or pos_bullet == 0:
                pos_bullet_rt = [x, y, bullet, 0, 0]
                pos_ast_destroy[2] = ast_dest
                time.sleep(0.7)
                pos_ast_destroy[3] = int(pos_ast_destroy[3]*1.2)
                pos_ast_destroy[4] = int(pos_ast_destroy[4]*1.2)
                time.sleep(0.5)
                bullet_ex = False
            else:
                pos_bullet_rt = [x, y, bullet, int(75 * mult), int(75 * mult)]
                time.sleep(0.25)
                if ON:
                    bullet_move(pos_bullet-1, mult-0.08, x, y)
                else:
                    pass

        def bullet_move_none(pos_bullet, mult, x, y):
            global pos_bullet_rt, bullet_ex, ON
            pos_bullet = int(pos_bullet)
            mult = float(mult)
            x = int(x)
            y = int(y)
            if pos_bullet == 0:
                bullet_ex = False
            else:
                pos_bullet_rt = [x, y, bullet, int(75 * mult), int(75 * mult)]
                time.sleep(0.25)
                if ON:
                    bullet_move_none(pos_bullet - 1, mult - 0.08, x, y)
                else:
                    pass

        def extra_bat(N):
            global RTHB, HBar
            N = int(N)
            if N <= 3:
                HBar += 1
                if HBar >= 6:
                    HBar = 6
                    RTHB = Healthbar[HBar]
                else:
                    RTHB = Healthbar[HBar]
            else:
                pass

        Thread(target=create_ast, args='9').start()
        Thread(target=create_bat).start()

        myfont = pg.font.SysFont('Times New Roman', 20)
        ON = True
        while ON:
            global ast, pos_ast, pos_bat, pos_bat_rt, posx_bat, posy_bat, pos_ast_destroy, pos_bullet_rt
            mouse = pg.mouse.get_pos()
            click = pg.mouse.get_pressed()
            gameDisplay.blit(game_bg, (0, 0))
            gameDisplay.blit(RTHB, (0, 0))
            text_sc = myfont.render('Puntaje: ' + str(score_player), False, (255, 255, 255))
            gameDisplay.blit(text_sc, (700,500))
            text_ce = myfont.render('Objetivos restantes: ' + str(ce1), False, (255, 255, 255))
            gameDisplay.blit(text_ce, (605,450))
            Hbar_aux += 1
            if ce1 != 0 and ast_ex:
                scale = pg.transform.scale(pos_ast_rt[2], (pos_ast_rt[3], pos_ast_rt[4]))
                gameDisplay.blit(scale, (pos_ast_rt[0], pos_ast_rt[1]))
            if bullet_ex:
                if not ast_ex:
                    if pos_bullet_rt[3] != 0:
                        scale = pg.transform.scale(pos_ast_destroy[2], (pos_ast_destroy[3], pos_ast_destroy[4]))
                        gameDisplay.blit(scale, (pos_ast_destroy[0], pos_ast_destroy[1]))
                        bullet_scale = pg.transform.scale(pos_bullet_rt[2], (pos_bullet_rt[3], pos_bullet_rt[4]))
                        gameDisplay.blit(bullet_scale, (pos_bullet_rt[0], pos_bullet_rt[1]))
                    else:
                        scale = pg.transform.scale(pos_ast_destroy[2], (pos_ast_destroy[3], pos_ast_destroy[4]))
                        gameDisplay.blit(scale, (pos_ast_destroy[0], pos_ast_destroy[1]))
                else:
                    bullet_scale = pg.transform.scale(pos_bullet_rt[2], (pos_bullet_rt[3], pos_bullet_rt[4]))
                    gameDisplay.blit(bullet_scale, (pos_bullet_rt[0], pos_bullet_rt[1]))
            if ON:
                scale_bat = pg.transform.scale(pos_bat_rt[2], (pos_bat_rt[3], pos_bat_rt[4]))
                gameDisplay.blit(scale_bat, (pos_bat_rt[0], pos_bat_rt[1]))
            if HBar <= 0:
                print('Game Over')
                ON = False
            if Hbar_aux == 210:
                HBar -= 1
                if HBar < 0:
                    HBar = 0
                    RTHB = Healthbar[HBar]
                else:
                    RTHB = Healthbar[HBar]
                    Hbar_aux = 0
            if pos_bat == 5:
                if mouse[0] in range(posx_bat - 73, posx_bat + 73) and mouse[1] in range(posy_bat - 70, posy_bat + 70) \
                        and hit_bat:
                    HBar += 1
                    hit_bat = False
                    if HBar >= 6:
                        HBar = 6
                        RTHB = Healthbar[HBar]
                    else:
                        RTHB = Healthbar[HBar]
            if pos_ast == 10:
                if mouse[0] in range(posx_ast - 73, posx_ast + 73) and mouse[1] in range(posy_ast - 70, posy_ast + 70) \
                        and hit:
                    HBar -= 2
                    hit = False
                    if HBar < 0:
                        HBar = 0
                        RTHB = Healthbar[HBar]
                    else:
                        RTHB = Healthbar[HBar]
                else:
                    pass
            if click[0] == 1 and not bullet_ex:
                if mouse[0] in range(posx_ast - 73, posx_ast + 73) and mouse[1] - 30 in range(posy_ast - 70,
                                                                                              posy_ast + 70):
                    pos_ast_destroy = pos_ast_rt
                    ast_ex = False
                    bullet_ex = True
                    scale = pg.transform.scale(pos_ast_destroy[2], (pos_ast_destroy[3], pos_ast_destroy[4]))
                    gameDisplay.blit(scale, (pos_ast_destroy[0], pos_ast_destroy[1]))
                    pos_bullet_rt = [mouse[0], mouse[1], bullet, 82, 82]
                    bullet_scale = pg.transform.scale(bullet, (82, 82))
                    gameDisplay.blit(bullet_scale, (mouse[0], mouse[1]))
                    Thread(target=bullet_move, args=('11', '1.1', str(mouse[0]), str(mouse[1]))).start()
                    Thread(target=extra_bat, args=(str(random.randint(0, 9)))).start()
                    score_player += 1
                else:
                    bullet_ex = True
                    pos_bullet_rt = [mouse[0], mouse[1], bullet, 82, 82]
                    bullet_scale = pg.transform.scale(bullet, (82, 82))
                    gameDisplay.blit(bullet_scale, (mouse[0], mouse[1]))
                    Thread(target=bullet_move_none, args=('11', '1.1', str(mouse[0]), str(mouse[1]))).start()
            if 0 <= pg.mouse.get_pos()[0] < 160:
                gameDisplay.blit(left2, mouse)
            if 160 <= pg.mouse.get_pos()[0] < 320:
                gameDisplay.blit(left1, mouse)
            if 320 <= pg.mouse.get_pos()[0] < 480:
                gameDisplay.blit(cntr, mouse)
            if 480 <= pg.mouse.get_pos()[0] < 640:
                gameDisplay.blit(right1, mouse)
            if 640 <= pg.mouse.get_pos()[0] <= 800:
                gameDisplay.blit(right2, mouse)
            for event in pg.event.get():
                if event.type == pg.QUIT:
                    ON = False
            pg.display.update()
            clock.tick(60)

        time.sleep(1.1)
        pg.quit()
        config.destroy()
        high_scores()
        score()

    def Game_ast_v():
        global List_Characters
        if not name_char:
            pass
        else:
            CP = List_Characters[0]
            List_Characters[0] = List_Characters[name_char[1]]
            List_Characters[name_char[1]] = CP
            List_Characters = List_Characters[1:]
            Game_ast()

    # ---------------------------------------------------Buttons--------------------------------------------------
    btn_cc = Button(config, text='Guardar Cambios', command=commit_changes, bg='black', fg='white')
    btn_cc.place(x=685, y=425)

    btn_back_config = Button(config, text='Atrás', command=back, bg='black', fg='white')
    btn_back_config.place(x=750, y=455)

    btn_ast = Button(config, text='Destrucción de Asteroides', command=Game_ast_v, bg='black', fg='white')
    btn_ast.place(x=640, y=395)

    btn_ring = Button(config, text='Maniobras', command=Game_ring_v, bg='black', fg='white')
    btn_ring.place(x=720, y=365)

    place_btn(0, 0, 0)

    config.mainloop()


# -----------------------------------------------Pantalla about-----------------------------------------------
def win_about():
    """
    Instituto Tecnológico de Costa Rica
    Ingeniería en Computadores
    Lenguaje: Python 3.6.4
    Autor: Iván Solís Ávila 2018209698
    Versión: 1.0.0
    Fecha de última modificación: 02/05/2018
    Entradas: no presenta
    Restricciones: no presenta
    Salidas: ventana de acerca del autor"""
    root.withdraw()
    win_about = Toplevel()
    win_about.title("Información")
    win_about.minsize(width=400, height=400)
    win_about.config(bg='white')

    def close_event():
        win_about.destroy()
        root.destroy()

    def back():
        global pausa
        pausa = True
        win_about.destroy()
        root.deiconify()

    win_about.protocol("WM_DELETE_WINDOW", close_event)

    lbl_about = Label(win_about, text=About, bg='white')
    lbl_about.pack()

    lbl_img_about = Label(win_about, image=load_img('Foto_about.gif'), bg='white')
    lbl_img_about.pack()

    btn_back2 = Button(win_about, text='Atrás', command=back, bg='white', fg='green')
    btn_back2.pack()

    win_about.mainloop()


# -------------------------------------------Pantalla instrucciones-------------------------------------------
def ventana_hs():
    """
    Instituto Tecnológico de Costa Rica
    Ingeniería en Computadores
    Lenguaje: Python 3.6.4
    Autor: Iván Solís Ávila 2018209698
    Versión: 1.0.0
    Fecha de última modificación: 02/05/2018
    Entradas: no presenta
    Restricciones: no presenta
    Salidas: highscores"""
    off()
    load_song("High_Scores.wav.wav")
    root.withdraw()
    ven_hs = Toplevel()
    ven_hs.title('Highscores')
    ven_hs.minsize(width=200, height=400)
    ven_hs.config(bg='grey')

    def back():
        ventana_hs.destroy()
        root.deiconify()
        off()
        load_song("Menu.wav.wav")

    def close_event():
        ventana_hs.destroy()
        root.deiconify()

        ventana_hs.protocol('WM_DELETE_WINDOW', close_event)

    lbl_ins = Label(ventana_hs, text=Instrucciones, font=('Times New Roman', 14), bg='grey')
    lbl_ins.pack()

    btn_back = Button(ventana_hs, text='Atrás', command=back, bg='white', fg='green')
    btn_back.place(x=50, y=50)

    ventana_hs.mainloop()

def salir_root():
    off()
    root.destroy()
# --------------------------------------------------Botones---------------------------------------------------
btn_juego = Button(c_root, text='Jugar', command=config, bg='grey')
btn_juego.place(x=827, y=320)

btn_about = Button(c_root, text='Información', command=win_about, bg='grey')
btn_about.place(x=791, y=360)

btnins = Button(c_root, text='Highscore', bg='grey')
btnins.place(x=800, y=400)

btn_salir = Button(c_root, text='Salir Juego', command=salir_root, bg='grey')
btn_salir.place(x=800, y=440)

root.mainloop()
