from tkinter import *
from threading import Thread
import threading
import winsound
import os
import time
from tkinter import messagebox
import random

global pausa
pausa=False

global posicion_obstaculos
posicion_obstaculos=[]

global numero_enemigos
numero_enemigos=[]

global puntuacion
puntuacion=0



def cargarImg(nombre):
    ruta=os.path.join('Imagenes',nombre)
    imagen=PhotoImage(file=ruta)
    return imagen

#Crear la ventana principal y nombrarla
root = Tk()
root.title('AVP')
root.minsize(640,640)
root.resizable(width=NO, height=NO)


#Funcion para cargar imagenes
C_root = Canvas(root, width=640,height=640,bg="black")
C_root.place(x=0,y=0)

fondo_root = cargarImg("Fondoroot.gif")
C_root.create_image(0,0,image = fondo_root,anchor = NW)

L_titulo = Label(C_root,text='Ingrese su nombre:',bg='white',fg='black')
L_titulo.place(x=50, y=550)
E_nombre = Entry(C_root,width=40)
E_nombre.place(x=50, y=525)

#Crear la Funcion del About
def About_open():
    root.withdraw()
    about = Toplevel()
    about.title('Informacion')
    about.minsize(800,600)
    about.resizable(width=NO,height=NO)

    C_about = Canvas(about, width=800,height=600, bg='black')
    C_about.place(x=0,y=0)

    CE=cargarImg("img3.gif")
    imagen_About=Label(about,bg='white')
    imagen_About.place(x=50,y=10)
    imagen_About.config(image=CE)


   #CAMBIAR ABOUT!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#
    aboutT="""
___________________________________________

Instituto Tecnologico de Costa Rica
Computer Engineering                              
                                            
Profesor:Milton Villegas Lemus
Autor:Santiago Gamboa Ramírez
Editor:Jonathan Esquivel Sanchez

Juego destrucción de misiles

El objetivo es destruir los misiles antes
de que destruyan la base
                                               
                                    
Version: 1.1.1                          
                                               
Nombre:Jonathan Esquivel Sanchez
Carnet:2018167983

____________________________________________

"""
    lbl_about = Label(C_about,text=aboutT,font=('Agency FB',14),bg='black',fg='white')
    lbl_about.place(x=400,y=10)

    
    def back():
        about.destroy()
        root.deiconify()

    Btn_back1 = Button(C_about,text='Atras',command=back,bg='blue',fg='white')
    Btn_back1.place(x=50,y=560)

    
    cancion1 = Thread(target=Song3,args=())
    cancion1.start()
    root.mainloop()

#Funcion Abrir ventana del Juego
def Juego_open(nombre,Disparos,Enemigos,Obstaculos):
    root.withdraw()
    juego = Toplevel()
    juego.title('AVP')
    juego.minsize(600,600)
    juego.resizable(width=NO,height=NO)

    fondo=Canvas(juego,width=600,height=600, bg='#000000')
    fondo.place(x=0,y=0)
    
    
    imagenGrid = cargarImg("Fondojuego.gif")
    fondo.create_image(0,0,image = imagenGrid, anchor = NW)


    L_nombre=Label(juego, text="Jugador: "+nombre, fg='black', bg='white')
    L_nombre.place(x=10,y=10)



    Predator = cargarImg("Predator.gif")
    PredatorI = cargarImg("Predatorlado2.gif")
    PredatorD = cargarImg("Predatorlado.gif")

    Alien = cargarImg("Alien.gif")
    AlienD = cargarImg("Alienlado.gif")
    AlienI = cargarImg("Alienlado2.gif")

    Obstaculo = cargarImg('Asteroid.gif')

    

    Bala = cargarImg("Bala.gif")



    tamano_cuadricula=50

    posicion_jugador_x= random.randint(0,11)
    posicion_jugador_y= random.randint(0,11)

    posicion_x=posicion_jugador_x*tamano_cuadricula
    posicion_y=posicion_jugador_y*tamano_cuadricula
        
    
#Crear jugador    
    fondo.create_image(posicion_x,posicion_y, image = Predator, anchor=NW, tags = ("pu", "player"), state = NORMAL)
    fondo.create_image(posicion_x,posicion_y, image = Predator, anchor=NW, tags = ("pd", "player"), state = HIDDEN)
    fondo.create_image(posicion_x,posicion_y, image = PredatorD, anchor=NW, tags = ("pr", "player"), state = HIDDEN)
    fondo.create_image(posicion_x,posicion_y, image = PredatorI, anchor=NW, tags = ("pl", "player"), state = HIDDEN)




    def len_list(Lista,R):
        if Lista==[]:
            return R
        else:
            return len_list(Lista[1:],R+1)
#Crear Obstaculos
    def obstaculos(Obstaculos,L,N):
        if Obstaculos==0:
            global posicion_obstaculos
            posicion_obstaculos=L
            pass
        elif L==[]:
            posplayer=fondo.coords('player')
            posobj=[random.randint(0,11)*50,random.randint(0,11)*50]
            if posobj!=posplayer:
                fondo.create_image(posobj[0],posobj[1],image = Obstaculo, anchor=NW, tags=('Obstaculo'+str(Obstaculos)), state=NORMAL)
                Obstaculos=Obstaculos-1
                L=L+[posobj]
                return obstaculos(Obstaculos,L,N)
            else:
                return obstaculos(Obstaculos,L,N)
        else:
            posplayer=fondo.coords('player')
            L=L+[obstaculos_aux(L,L,Obstaculos,[random.randint(0,11)*50,random.randint(0,11)*50],posplayer)]
            Obstaculos=Obstaculos-1
            return obstaculos(Obstaculos,L,N+1)

    def obstaculos_aux(L,L2,NO,posobj,P):
        if L==[]:
            fondo.create_image(posobj[0],posobj[1],image = Obstaculo, anchor=NW, tags=('Obstaculo'+str(NO)), state=NORMAL)
            return posobj
        elif posobj==L[0] or posobj==P:
            L=L2
            return obstaculos_aux(L,L2,NO,[random.randint(0,11)*50,random.randint(0,11)*50],P)
        else:
            return obstaculos_aux(L[1:],L2,NO,posobj,P)

    salir_obstaculos=Thread(target=obstaculos,args=(Obstaculos,[],0))
    salir_obstaculos.start()
    
#Crear Enemigo
    def enemigo_aux(pos,L):
        if L==[]:
            return True
        elif pos==L[0]:
            return False
        else:
            return enemigo_aux(pos,L[1:])
        

    def enemigo2(N,L,NE,pos,C):
        posplayer=fondo.coords('player')
        global posicion_obstaculos
        posobj=posicion_obstaculos
        if N==len(posicion_obstaculos):
            P=enemigo_aux(pos,L)
            if P==True:
                fondo.create_image(pos[0],pos[1],image = Alien, anchor=NW,tags = ("EU"+str(NE),'Enemigo'+str(NE)),state = NORMAL)
                fondo.create_image(pos[0],pos[1],image = Alien, anchor=NW,tags = ("ED"+str(NE),'Enemigo'+str(NE)),state = HIDDEN)
                fondo.create_image(pos[0],pos[1],image = AlienD, anchor=NW,tags = ("ER"+str(NE),'Enemigo'+str(NE)),state = HIDDEN)
                fondo.create_image(pos[0],pos[1],image = AlienI, anchor=NW,tags = ("EL"+str(NE),'Enemigo'+str(NE)),state = HIDDEN)
                crear_Threads(NE)
                global numero_enemigos
                numero_enemigos=numero_enemigos+[C]
                time.sleep(5)
                return pos
            else:
                pos=[random.randint(0,11)*50,random.randint(0,11)*50]
                return enemigo2(0,L,NE,pos)
        elif pos==posobj[N] or pos==posplayer or pos == [0,0] or pos == [0,550] or pos == [550,0] or pos == [550,550]:
            pos=[random.randint(0,11)*50,random.randint(0,11)*50]
            return enemigo2(0,L,NE,pos)
        else:
            return enemigo2(N+1,L,NE,pos)
    

    def enemigo(Enemigos,L,C):
        if Enemigos==0:
            global numero_enemigos
            numero_enemigos=len_list(L,[],1)
            print(numero_enemigos)
        else:
            L=L+[enemigo2(0,L,Enemigos,[random.randint(0,11)*50,random.randint(0,11)*50],C)]
            C+=1
            Enemigos=Enemigos-1
            return enemigo(Enemigos,L)

    def numero_list(L,K,N):
        if L==[]:
            return K
        else:
            K=K+[N]
            return numero_list(L[1:],K,N+1)

    salir_enemigos=Thread(target=enemigo,args=(Enemigos,[],1))
    salir_enemigos.start()

 #   def mover_alienll(M,N):
#        pos=fondo.coords("player")
 #       posenemigomov=fondo.coords("Enemigo"+str(M))
  #      posenemigo=fondo.coords("Enemigo"+str(N))
   #     global posicion_obstaculos
    #    posobstaculos=poscicion_obstaculos
     #   if N==(len(posicion_enemigos)):
      #      mover_alien_aux()
       # elif posenemigomov==posenemigo
        

    def crear_Threads(N):
        movimiento_enemigo=Thread(target=mover_alien,args=(str(N)))
        movimiento_enemigo.start()
        


    def mover_alien(NE):
        global pausa
        if pausa==False:
            NE=int(NE)
            pos=fondo.coords("player")
            global numero_enemigos
            if fondo.coords("Enemigo"+str(NE)) == []:
                pass
            else:
                posenemigo=fondo.coords("Enemigo"+str(NE))
                l=[50,-50]
                l=l[random.randint(0,1)]
                m=random.randint(1,2)
                if m==1 and 0<posenemigo[0]<550:
                    posenemigo=[posenemigo[0]+l,posenemigo[1]]
                    if posenemigo==pos or posenemigo == [0,0] or posenemigo == [0,550] or posenemigo == [550,0] or posenemigo == [550,550]:
                        return mover_alien(NE)
                    else:
                        return colision_alien_obstaculos(0,posenemigo,NE,l,m)
                elif m==2 and 0<posenemigo[1]<550:
                    posenemigo=[posenemigo[0],posenemigo[1]+l]
                    if posenemigo==pos or posenemigo == [0,0] or posenemigo == [0,550] or posenemigo == [550,0] or posenemigo == [550,550]:
                        return mover_alien(NE)
                    else:
                        return colision_alien_obstaculos(0,posenemigo,NE,l,m)

                else:
                    mover_alien(NE)
        else:
            time.sleep(1)
            return mover_alien(NE)
                


    def colision_alien_obstaculos(N,posenemigo,NE,L,M):
        global pausa
        if pausa==False:
            global posicion_obstaculos
            posobstaculos=posicion_obstaculos
            if N==len(posicion_obstaculos):
                return colision_alien_enemigos(1,posenemigo,NE,L,M)
            elif posenemigo==posobstaculos[N]:
                return mover_alien(NE)
            else:
                return colision_alien_obstaculos(N+1,posenemigo,NE,L,M)
        else:
            time.sleep(1)
            return colision_alien_obstaculos(N,posenemigo,NE,L,M)
        

    def colision_alien_enemigos(N,posenemigo,NE,L,M):
        global pausa
        if pausa==False:
            global numero_enemigos
            posenemigos=fondo.coords("Enemigo"+str(N))
            print(posenemigos)
            if N==len(numero_enemigos):
                if M==1:
                    if L>0:
                        fondo.move("Enemigo"+str(NE),L,0)
                        fondo.itemconfig('EU'+str(NE), state=HIDDEN)
                        fondo.itemconfig('ED'+str(NE), state=HIDDEN)
                        fondo.itemconfig('ER'+str(NE), state=NORMAL)
                        fondo.itemconfig('EL'+str(NE), state=HIDDEN)
                        time.sleep(3)
                        return mover_alien(NE)
                    else:
                        fondo.move("Enemigo"+str(NE),L,0)
                        fondo.itemconfig('EU'+str(NE), state=HIDDEN)
                        fondo.itemconfig('ED'+str(NE), state=HIDDEN)
                        fondo.itemconfig('ER'+str(NE), state=HIDDEN)
                        fondo.itemconfig('EL'+str(NE), state=NORMAL)
                        time.sleep(3)
                        return mover_alien(NE)
                        
                else:
                    if L>0:
                        fondo.move("Enemigo"+str(NE),0,L)
                        fondo.itemconfig('EU'+str(NE), state=HIDDEN)
                        fondo.itemconfig('ED'+str(NE), state=NORMAL)
                        fondo.itemconfig('ER'+str(NE), state=HIDDEN)
                        fondo.itemconfig('EL'+str(NE), state=HIDDEN)
                        time.sleep(3)
                        return mover_alien(NE)
                    else:
                        fondo.move("Enemigo"+str(NE),0,L)
                        fondo.itemconfig('EU'+str(NE), state=NORMAL)
                        fondo.itemconfig('ED'+str(NE), state=HIDDEN)
                        fondo.itemconfig('ER'+str(NE), state=HIDDEN)
                        fondo.itemconfig('EL'+str(NE), state=HIDDEN)
                        time.sleep(3)
                        return mover_alien(NE)
            elif posenemigo==posenemigos:
                return mover_alien(NE)
            else:
                return colision_alien_enemigos(N+1,posenemigo,NE,L,M)
        else:
            time.sleep(1)
            return colision_alien_enemigos(N,posenemigo,NE,L,M)



        
    def colision_enemigo_arriba(N):
        global numero_enemigos
        pos=fondo.coords('player')
        posenemigo=fondo.coords("Enemigo"+str(N))
        if N==(len(numero_enemigos)+1):
            fondo.move("player",0,-mov_y)
        elif [pos[0],pos[1]-50]==posenemigo:
              pass
        else:
              return colision_enemigo_arriba(N+1)

    def colision_enemigo_abajo(N):
        global numero_enemigos
        pos=fondo.coords('player')
        posenemigo=fondo.coords("Enemigo"+str(N))
        if N==(len(numero_enemigos)+1):
            fondo.move("player",0,mov_y)
        elif [pos[0],pos[1]+50]==posenemigo:
              pass
        else:
              return colision_enemigo_abajo(N+1)

    def colision_enemigo_izquierda(N):
        global numero_enemigos
        pos=fondo.coords('player')
        posenemigo=fondo.coords("Enemigo"+str(N))
        if N==(len(numero_enemigos)+1):
            fondo.move("player",-mov_x,0)
        elif [pos[0]-50,pos[1]]==posenemigo:
              pass
        else:
              return colision_enemigo_izquierda(N+1)

    def colision_enemigo_derecha(N):
        global numero_enemigos
        pos=fondo.coords('player')
        posenemigo=fondo.coords("Enemigo"+str(N))
        if N==(len(numero_enemigos)+1):
            fondo.move("player",mov_x,0)
        elif [pos[0]+50,pos[1]]==posenemigo:
              pass
        else:
              return colision_enemigo_derecha(N+1)


#    def colision_obstaculo(posplayer,posobst):
#        pos=fondo.coords("Player")
#        len(posobst)
#        if len(posobst)==0:
#            colision_enemigos(posplayer,5)
#        elif posobst[0]==pos:
#            fondo.move("Player",posplayer[0],posplayer[1])
#            print(posobst)
#        else:
#            return colision_obstaculo(posplayer,posobst[1:])
        

#    def colision_enemigos(posplayer,NE):
#        pos=fondo.coords("Player")
#        posenemigo=fondo.coords("Enemigos"+str(NE))
#        if NE==0:
#            pass
#        elif pos==posenemigo:
#            fondo.move("Player",posplayer[0],posplayer[1])
#        else:
#            return colision_enemigos(posplayer,NE-1)



    mov_x=50
    mov_y=50

           
    


    def colisionbala(N):
        pos=fondo.coords('Bala')
        global posicion_obstaculos
        posobj=posicion_obstaculos
        if N==len(posicion_obstaculos):
            return True
        elif pos==posobj[N]:
            fondo.delete("Bala")
            global pausa
            pausa=False
        else:
            N=N+1
            return colisionbala(N)
        


    def colision_arriba(N):
        pos=fondo.coords('player')
        global posicion_obstaculos
        posobj=posicion_obstaculos
        if N==len(posicion_obstaculos):
            return colision_enemigo_arriba(0)
        elif [pos[0],pos[1]-50]==posobj[N]:
            pass
        else:
            return colision_arriba(N+1)

        
    def colision_abajo(N):
        pos=fondo.coords('player')
        global posicion_obstaculos
        posobj=posicion_obstaculos
        if N==len(posicion_obstaculos):
            return colision_enemigo_abajo(0)
        elif [pos[0],pos[1]+50]==posobj[N]:
            pass
        else:
            return colision_abajo(N+1)


    def colision_izquierda(N):
        pos=fondo.coords('player')
        global posicion_obstaculos
        posobj=posicion_obstaculos
        if N==len(posicion_obstaculos):
            return colision_enemigo_izquierda(0)
        elif [pos[0]-50,pos[1]]==posobj[N]:
            pass
        else:
            return colision_izquierda(N+1)


    def colision_derecha(N):
        pos=fondo.coords('player')
        global posicion_obstaculos
        posobj=posicion_obstaculos
        if N==len(posicion_obstaculos):
            return colision_enemigo_derecha(0)
        elif [pos[0]+50,pos[1]]==posobj[N]:
            pass
        else:
            return colision_derecha(N+1)
        
    
    
    def mover(event):
        pos=fondo.coords("player")
        Key=event.char
        global pausa
        if pausa==False:
            if (Key=="w") or (Key=="W"):
                if  pos[1]>0:
                    colision_arriba(0)
                else:
                    pass
            elif (Key=="a") or (Key=="A"):
                if pos[0]>0:
                    colision_izquierda(0)
                else:
                    pass
            elif (Key=="s") or (Key=="S"):
                if pos[1]<550:
                    colision_abajo(0)
                else:
                    pass
            elif (Key=="d")or (Key=="D"):
                if pos[0]<550:
                    colision_derecha(0)
                else:
                    pass
            elif (Key=="u") or (Key=="U"):
                fondo.itemconfig('pu', state=NORMAL)
                fondo.itemconfig('pd', state=HIDDEN)
                fondo.itemconfig('pr', state=HIDDEN)
                fondo.itemconfig('pl', state=HIDDEN)
            elif (Key=="h") or (Key=="H"):
                fondo.itemconfig('pu', state=HIDDEN)
                fondo.itemconfig('pd', state=HIDDEN)
                fondo.itemconfig('pr', state=HIDDEN)
                fondo.itemconfig('pl', state=NORMAL)
            elif (Key=="j") or (Key=="J"):
                fondo.itemconfig('pu', state=HIDDEN)
                fondo.itemconfig('pd', state=NORMAL)
                fondo.itemconfig('pr', state=HIDDEN)
                fondo.itemconfig('pl', state=HIDDEN)
            elif (Key=="k") or (Key=="K"):
                fondo.itemconfig('pu', state=HIDDEN)
                fondo.itemconfig('pd', state=HIDDEN)
                fondo.itemconfig('pr', state=NORMAL)
                fondo.itemconfig('pl', state=HIDDEN)
            elif (Key=="g") or (Key=="G"):
                pausa=True
                disparar(Disparos)
            elif (Key=="p"):
                enemigo2(0,[],45,[0,0])
            else:
                pass
        else:
            pass


    def disparar(Disparos):
        global puntuacion
        if Disparos==0:
            pass
        else:
            pos=fondo.coords("player")
            U=fondo.itemcget('pu','state')
            D=fondo.itemcget('pd','state')
            R=fondo.itemcget('pr','state')
            L=fondo.itemcget('pl','state')
            if U=='normal':
                fondo.create_image(pos[0], pos[1], image=Bala, anchor=NW, tags= "Bala", state=NORMAL)
                puntuacion=puntuacion+mover_bala_arriba(0)
            elif D=='normal':
                fondo.create_image(pos[0], pos[1], image=Bala, anchor=NW, tags= "Bala", state=NORMAL)
                puntuacion=puntuacion+mover_bala_abajo(0)
            elif R=='normal':
                fondo.create_image(pos[0], pos[1], image=Bala, anchor=NW, tags= "Bala", state=NORMAL)
                puntuacion=puntuacion+mover_bala_derecha(0)
            else:
                fondo.create_image(pos[0], pos[1], image=Bala, anchor=NW, tags= "Bala", state=NORMAL)
                puntuacion=puntuacion+mover_bala_izquierda(0)


    def score_aux(N):
        posbala=fondo.coords("Bala")
        posenemigo=fondo.coords("Enemigo"+str(N))
        if N==0:
            return 0
        elif posbala==posenemigo:
            fondo.delete("Enemigo"+str(N))
#            global posicion_enemigos
#            enemigos=posicion_enemigos
#            posicion_enemigos=elim_list(0,N,enemigos)
            return 1
        else:
            return score_aux(N-1)


#    def elim_list(E,N,K):
#       if E==N:
#           return K[1:]
#        else:
#            return [K[0]]+elim_list(E+1,N,K[1:])

    
    
    def mover_bala_arriba(X):
        pos=fondo.coords("Bala")
        global numero_enemigos
        if pos[1]<=0:
            fondo.delete("Bala")
            global pausa
            pausa=False
            return X**2
        else:
            fondo.move("Bala",0,-mov_y)
            X=X+score_aux(len(numero_enemigos))
            time.sleep(0.2)
            fondo.update()
            C=colisionbala(0)
            if C==True:
                return mover_bala_arriba(X)
            else:
                return X**2

        
    def mover_bala_abajo(X):
        pos=fondo.coords("Bala")
        global numero_enemigos
        if pos[1]>550:
            fondo.delete("Bala")
            global pausa
            pausa=False
            return X**2
        else:
            fondo.move("Bala",0,mov_y)
            X=X+score_aux(len(numero_enemigos))
            time.sleep(0.2)
            fondo.update()
            C=colisionbala(0)
            if C==True:
                return mover_bala_abajo(X)
            else:
                return X**2


    def mover_bala_izquierda(X):
        pos=fondo.coords("Bala")
        global numero_enemigos
        if pos[0]<=0:
            fondo.delete("Bala")
            global pausa
            pausa=False
            return X**2
        else:
            fondo.move("Bala",-mov_x,0)
            X=X+score_aux(len(numero_enemigos))
            time.sleep(0.2)
            fondo.update()
            C=colisionbala(0)
            if C==True:
                return mover_bala_izquierda(X)
            else:
                return X**2


    def mover_bala_derecha(X):
        pos=fondo.coords("Bala")
        global numero_enemigos
        if pos[0]>550:
            fondo.delete("Bala")
            global pausa
            pausa=False
            return X**2
        else:
            fondo.move("Bala",mov_x,0)
            X=X+score_aux(len(numero_enemigos))
            time.sleep(0.2)
            fondo.update()
            C=colisionbala(0)
            if C==True:
                return mover_bala_derecha(X)
            else:
                return X**2
            
        
    juego.bind("<w>",mover)
    juego.bind("<W>",mover)
    juego.bind("<a>",mover)
    juego.bind("<A>",mover)
    juego.bind("<s>",mover)
    juego.bind("<S>",mover)
    juego.bind("<d>",mover)
    juego.bind("<D>",mover)
    juego.bind("<u>",mover)
    juego.bind("<U>",mover)
    juego.bind("<h>",mover)
    juego.bind("<H>",mover)
    juego.bind("<j>",mover)
    juego.bind("<J>",mover)
    juego.bind("<k>",mover)
    juego.bind("<K>",mover)
    juego.bind("<g>",mover)
    juego.bind("<G>",mover)
    juego.bind("<p>",mover)
    
    
    
    def back():
        global posicion_obstaculos
        posicion_obstaculos=[]
        global numero_enemigos
        print(numero_enemigos)
        numero_enemigos=[]
        global puntuacion
        print(puntuacion)
        puntuacion=0
        juego.destroy()
        root.deiconify()

    Btn_back_juego = Button(fondo,text='Atras',command=back,bg='blue',fg='white')
    Btn_back_juego.place(x=50,y=550)

    juego.mainloop()


#Mover Imagenes
    
#Funcion de los botones
def About():
    About_open()
    
def JuegoFacil():
    Disparos=5
    Enemigos=6
    Obstaculos=8
    nombre = str(E_nombre.get())
    if nombre=="":
        print("El nombre no puede ser vacio")
    else:
        Juego_open(nombre,Disparos,Enemigos,Obstaculos)

def JuegoNormal():
    Disparos=5
    Enemigos=9
    Obstaculos=10
    nombre = str(E_nombre.get())
    if nombre=="":
        print("El nombre no puede ser vacio")
    else:
        Juego_open(nombre,Disparos,Enemigos,Obstaculos)

def JuegoDificil():
    Disparos=5
    Enemigos=12
    Obstaculos=14
    nombre = str(E_nombre.get())
    if nombre=="":
        print("El nombre no puede ser vacio")
    else:
        Juego_open(nombre,Disparos,Enemigos,Obstaculos)
    
#Poner Botones del root
Btn_About = Button(C_root,text='About',command=About,fg='white',bg='blue')
Btn_About.place(x=450, y=50)

imagenFacil=cargarImg("Facil.gif")
Btn_Juego = Button(C_root,image=imagenFacil,command=JuegoFacil,fg='black',bg="green")
Btn_Juego.place(x=300, y=550)

imagenNormal=cargarImg("Normal.gif")
Btn_Juego = Button(C_root,image=imagenNormal,command=JuegoNormal,fg='black',bg="blue")
Btn_Juego.place(x=400, y=550)

imagenDificil=cargarImg("Dificil.gif")
Btn_Juego = Button(C_root,image=imagenDificil,command=JuegoDificil,fg='black',bg="red")
Btn_Juego.place(x=500, y=550)

root.mainloop()

colision_arriba(0)
